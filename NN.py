from keras.layers import Dense
from keras.models import Sequential
from sklearn.cluster import KMeans
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics.pairwise import cosine_similarity
import warnings
warnings.filterwarnings('ignore')


class User:
	def __init__(self, id):
		self.id = id
		self.movieObjects = []
	
	def updateMovieObjects(self, id, rating, timestamp):
		self.movieObjects.append((str(id), float(rating), str(timestamp)))

class Movie:
	def __init__(self, id, title, genre):
		self.id = id
		self.title = title
		self.genre = genre

class UserCluster:
	def __init__(self, id):
		self.id = id
		self.memberUsers = []


def nearest(x):
	if(x > 5.0):
		return 5.0
	lower_delta = x - int(x)
	mid_delta = abs(x - 0.5 - int(x))
	upper_delta = int(x) + 1 - x
	if(lower_delta < upper_delta):
		if(lower_delta < mid_delta):
			return int(x)
		else:
			return int(x) + 0.5
	else:
		if(upper_delta < mid_delta):
			return int(x) + 1
	return int(x) + 0.5


def populate(users, movies, userID, closeList):
	X = []
	y = []
	user = users[userID]
	for i in range(len(user.movieObjects)/2):
		X.append(movies[user.movieObjects[i][0]].genre)
		y.append(user.movieObjects[i][1])
	u1 = users[closeList[1][1]]
	u2 = users[closeList[2][1]]
	for i in range(len(u1.movieObjects)/4):
		X.append(movies[u1.movieObjects[i][0]].genre)
		up = u1.movieObjects[i][1]/closeList[1][0]
		down = u1.movieObjects[i][1]*closeList[1][0]
		avg = (up + down)/2
		y.append(nearest(avg[0][0]))
	for i in range(len(u2.movieObjects)/4):
		X.append(movies[u2.movieObjects[i][0]].genre)
		up = u2.movieObjects[i][1]/closeList[2][0]
		down = u2.movieObjects[i][1]*closeList[2][0]
		avg = (up + down)/2
		y.append(nearest(avg[0][0]))
	return X, y

userData = [line.rstrip('\n') for line in open('ratings.csv')]
userData = [line.rstrip('\r') for line in userData]
userData = [data.split(',') for data in userData]


users = {}
for data in userData:
	userID = data[0]
	movieID = data[1]
	rating = data[2]
	timestamp = data[3]
	if(not userID in users):
		users[userID] = User(userID)
	users[userID].updateMovieObjects(movieID, rating, timestamp)


movieData = [line.rstrip('\n') for line in open('movies.csv')]
movieData = [line.rstrip('\r') for line in movieData]
movieData = [data.split(',') for data in movieData]


movies = {}
for data in movieData:
	movieID = data[0]
	title = ''.join(data[1:-1])
	genre = data[-1].split('|')
	if("(no genres listed)" in genre):
		continue
	movies[movieID] = Movie(movieID, title, genre)

genres = []
for movieID in movies:
	movie = movies[movieID]
	for g in movie.genre:
		if(not g in genres):
			genres.append(g)


for movieID in movies:
	movie = movies[movieID]
	sparseGenre = []
	for g in genres:
		if(g in movie.genre):
			sparseGenre.append(1)
		else:
			sparseGenre.append(0)
	movie.genre = sparseGenre


X = []
for userID in users:
	user = users[userID]
	user.genres = [0]*19
	for movieObject in user.movieObjects:
		if(movieObject[0] in movies):
			for i in range(len(user.genres)):
				user.genres[i] += movies[movieObject[0]].genre[i]
	s = sum(user.genres)*1.0
	user.genres = [ug/s for ug in user.genres]
	X.append(user.genres)


kmeans = KMeans(n_clusters=5, random_state=0).fit(X)
clusters = {}
for userID in users:
	user = users[userID]
	cLabel = kmeans.predict(user.genres)[0]
	if(not cLabel in clusters):
		clusters[cLabel] = UserCluster(cLabel)
	clusters[cLabel].memberUsers.append(userID)


for userID in users:
	user = users[userID]
	cLabel = kmeans.predict(user.genres)[0]
	closeList = []
	for uID in clusters[cLabel].memberUsers:
		sim = cosine_similarity(user.genres, users[uID].genres)
		closeList.append((sim, uID))
	closeList.sort(key=lambda x: x[0], reverse = True)

	X, Y = populate(users, movies, userID, closeList)
	encoder = LabelEncoder()
	encoder.fit(Y)
	encoded_Y = encoder.transform(Y)
	dummy_y = np_utils.to_categorical(encoded_Y, nb_classes=11)


	model = Sequential()
	model.add(Dense(40, input_dim=19, init='uniform', activation='relu'))
	model.add(Dense(28, init='uniform', activation='relu'))
	model.add(Dense(11, init='uniform', activation='sigmoid'))
	model.compile(loss='binary_crossentropy', optimizer='adam')
	
	model.fit(X, dummy_y, nb_epoch=150, batch_size=10, verbose=0)
	X1 = []
	y1 = []
	for i in range(len(user.movieObjects)/2 + 1, len(user.movieObjects)):
		X1.append(movies[user.movieObjects[i][0]].genre)
		y1.append(user.movieObjects[i][1])
	
	predictions = model.predict_classes(X1, batch_size=32, verbose=0)
	predictions = encoder.inverse_transform(predictions)
	for i in range(len(predictions)):
		print(str(y1[i]) + '   ' + str(predictions[i]))
	break
	